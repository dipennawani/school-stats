import csv
import datetime

def countMatchingWords(schoolName, searchWords):
	countOfMatchingStrings = 0
	for word in searchWords:
		if word in schoolName:
			countOfMatchingStrings = countOfMatchingStrings + 1
	return countOfMatchingStrings

def search_schools(searchString):

	a = datetime.datetime.now()
	searchWords = searchString.lower().split(' ');
	
	file =  open('schooldata.csv', 'r')
	reader = csv.reader(file, delimiter=',')
	next(reader, None)
	
	matchingRecords = {}
	for row in reader:		
		schoolName = row[3].lower()
		
		totalMatches = countMatchingWords(schoolName,searchWords)
		
		if totalMatches > 0:
			matchingRecords[row[3] +"\n" + row[4] + ", " + row[5]] = totalMatches
	
	matchingRecords = sorted(matchingRecords.items(), key=lambda x: x[1], reverse=True)
	totalMatchingRecords = len(matchingRecords)
	b = datetime.datetime.now()
	c = b - a
	print('Results for "' + searchString + '" (search took: '+ str(c.total_seconds()) + 's)')
	for i in range(3):
		if i < totalMatchingRecords:
			print(str(i+1) + '. ' + matchingRecords[i][0])
search_schools("elementary school highland park")
search_schools("jefferson belleville")
search_schools("riverside school 44")
search_schools("granada charter school")
search_schools("foley high alabama")
search_schools("KUSKOKWIM")