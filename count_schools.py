import csv

def print_counts():
	file =  open('schooldata.csv', 'r')
	reader = csv.reader(file, delimiter=',')
	next(reader, None)
		
	stateCounts = {}
	metroCCounts = {}
	cityCounts = {}
	totalSchools = 0
	for row in reader:
		totalSchools = totalSchools + 1
		state = row[5]
		if state in stateCounts:
			stateCounts[state] = stateCounts[state] + 1
		else:
			stateCounts[state] = 1
		
		metroCLocale = row[8]
		if metroCLocale in metroCCounts:
			metroCCounts[metroCLocale] = metroCCounts[metroCLocale] + 1
		else:
			metroCCounts[metroCLocale] = 1
		
		city = row[4]
		if city in cityCounts:
			cityCounts[city] = cityCounts[city] + 1
		else:
			cityCounts[city] = 1
	
	print("Total Schools: " + str(totalSchools))
	print("Schools by State:")
	
	for state in stateCounts:
		print(state+": " + str(stateCounts[state]))
	
	print("Schools by Metro-centric locale:")
	for metroCLocale in metroCCounts:
		print(metroCLocale+": " + str(metroCCounts[metroCLocale]))
	
	cityWithMaxSchools = ''
	cityMaxCount = 0
	uniqueCities = 0	
	for city in cityCounts:
		uniqueCities = uniqueCities + 1
		cityCount = cityCounts[city]
		if cityCount> cityMaxCount:
			cityMaxCount = cityCount
			cityWithMaxSchools = city
	print("City with most schools: "+ cityWithMaxSchools + " ("+ str(cityMaxCount) + " schools)")
	print("Unique cities with at least one school: " + str(uniqueCities))
print_counts()